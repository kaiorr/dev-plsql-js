# LifeApps - Processo seletivo - Desenvolvedor PL/SQL

## Objetivo
Esse repositório descreve um exercício prático no uso de PL/SQL para avaliar a familiaridade do candidato com o uso dessa ferramenta.

## Critérios de sucesso
- Uma execução de sucesso desse desafio deve seguir todas as premissas estabelecidas nesse README.md e o uso das fontes de dados apresentadas como obrigatórias.
- As partes obrigatórias do desafio devem ser executadas em sua completude.
- Qualquer parte explicitamente marcada como opcional pode ser executada totalmente ou deixada de fora pelo candidato. Caso uma parte opcional fique incompleta ou execute incorretamente, deixe explícito para que a análise leve isso em consideração. Esse cenário não desclassificará o candidato se assim apontado.
- A entrega deve ser feita em um repositório no Gitlab, como um fork/duplicação deste repositório. A URL do repositório deve ser informada aos Talentos Humanos responsáveis pela condução da seleção.
- Todos os testes estão sujeitos a receber colunas com valores nulos ou vazios. Trate esses casos

### Versão do Oracle
A sintaxe em uso deve ser do Oracle 10g ou inferior. Recursos mais recentes que essa versão não devem ser usados.

## Recursos disponíveis
- O desafio se iniciará em um banco de dados Oracle que tenha executado os scripts presentes na pasta desafio/01-recursos. A ordem de execução inicial dos scripts é irrelevante, mas todos devem ser executados com sucesso para montagem do cenário inicial.

### Cenário inicial
- No cenário inicial existe uma tabela chamada `pedidos_recebidos`, emulando pedidos antigos que tenham seus dados consolidados por outro sistema. A consolidação dos dados dessa tabela concatenou alguns dados e combinou, na mesma estrutura, dados do pedido e do endereço para tornar procedimentos de consulta mais eficientes. A estrutura dessa tabela pode ser conferida no Anexo I.

- Para construir novos recursos, foi solicitado que esses dados sejam parcialmente normalizados de forma automática. Sempre que um novo registro for inserido em `pedidos_recebidos`, algumas ações devem ocorrer:
  1.  (`obrigatório`) - Deve ser feita uma inserção na tabela `cidades_com_pedidos` (definição de colunas abaixo). 
      - Caso o registro já exista, nada deve ser feito. A existência do registro é confirmada se todas as colunas da tabela estiverem com o mesmo valor do novo registro à ser inserido.
      - Caso o registro não exista, inserir a cidade e a UF
      - Caso o dado a ser inserido não tenha a cidade ou a UF, ignorar a inserção
       - Você **não deve** apagar a tabela `cidades_com_pedidos` e recalcula-la por completo em nenhuma hipótese.
  2. (`obrigatório`) - Deve ser feita uma inserção ou atualização na tabela `total_pedidos_data`, que agrega o total vendido por dia
      - O dia começa na meia noite daquela data e termina às 23h59min59s, horário de Brasília
      - Unidades de tempo menor que segundo não são usadas. Por exemplo: não será utilizado mili ou microsegundo na expressão das datas.
      - Caso o registro para o dia da nova data já exista, ele deve atualizado somando o total anterior (já gravado na tabela) com o do pedido sendo processado no momento.
      - Caso o registro para o dia da nova data não exista, ele deve ser criado com o total do pedido sendo processado no momento.
      - Você **não deve** apagar a tabela `total_pedidos_data` e recalcula-la por completo em nenhuma hipótese.
      - A data a ser gravada em `total_pedidos_data` deve neutralizar a hora, deixando somente até o nível do dia.
        - Exemplo: se a tabela `total_pedidos_data` está vazia e são recebidos três novos pedidos com datas e valores como abaixo:

        > *(Valores de entrada já extraídos da tabela pedidos_recebidos)*

        | valor | data |
        |--|--|
        | 200.50 | 2020-03-01T23:32:11-0300 |
        | 100.53 | 2020-03-01T13:02:00-0300 |
        | 300.50 | null |
        | 300.50 | 2020-03-02T08:20:05-0300 |
        
        
        **Resultado:** A tabela `total_pedidos_data` deverá finalizar com

        | data_dia | total_vendido |
        |--|--|
        | 2020-03-01T00:00:00-0300 | 301.03 |
        | 2020-03-02T00:00:00-0300 | 300.50 |

        Essa tabela, alimentada automaticamente, deve conseguir responder a pergunta: "Qual o total vendido no dia X?" sem que seja preciso recorrer à uma função de agregação, como o 'sum()'

  3. (`obrigatório`) Um pedido pode ter tido ou não tentativas de pagamento. Elas devem ser gravadas na tabela `tentativas_pagamento` (definição completa no Anexo IV). Cada registro na tabela `pedidos_recebidos` coluna `tentativas_pagamento`, pode conter zero, uma ou multiplas tentativas de pagamento. As tentativas são separadas por virgula (,) e, para cada uma, existe um número de controle e um indicador de sucesso (valores: `ok`, `error`). Esses dois dados são separados por ponto-e-virgula (:).

    - Exemplos de entradas válidas para serem processadas:

      | tentativas_pagamento |
      |--|
      |9000:error,9001:error,9002:error,9010:no|
      |9000:err|
      |9000:error,9001:error|
      > O exemplo acima somente mostra a coluna de tentativas de pagamento. A tabela de pedidos tem sua estrutura especificada no Anexo I.
    
    - Para cada nova entrada em `pedidos_recebidos`, verifique se existem tentativas de pagamento e, existindo, **utilize um laço procedural** (`obrigatório`) para realizar a inserção dos registros.
    - Você não deve escrever um único insert que adicione todas as linhas na tabela `tentativas_pagamento`
    - Você não deve apagar a tabela `tentativas_pagamento` e recalcula-la por completo.
    - Você **não deve** apagar a tabela `tentativas_pagamento` e recalcula-la por completo em nenhuma hipótese.
  4. (`opcional`) Este item é `opcional` e desejável. Sua implementação ou não fica a cargo do candidato: para cada uma das ações acima, desejá-se um registro de log que informe se ela teve sucesso, falha parcial, falha total. Esse registro deverá ser feito na tabela `logs_processamento_pedido`, documentada no Anexo V. 
    - Deve ser criada uma função independente, com **sessão própria** que grave os resultados das importacoes para cada tabela de saída dos itens anteriores (`cidades_com_pedidos`, `total_pedidos_data` e `tentativas_pagamento`). Para cada item que tentou ser processado, inserir uma linha na tabela de logs com o campo `texto_operacao` indicando o que se está tentando fazer e o campo `resultado_operacao` indicando o resultado.
      - Exemplos: 

      |data_log|texto_operacao|resultado_operacao|
      |--|--|--|
      |2020-03-01T23:01:00|Novo cidades_com_pedidos com (cidade=Vitória,uf=ES)|Cidade já existia: ignorado|
      |2020-03-01T23:02:00|Novo cidades_com_pedidos com (cidade=Manaus,uf=AM)|Cidade nova. Tentativa de criação. Criação com sucesso|
      |2020-03-01T23:03:00|Novo cidades_com_pedidos com (cidade=Manaus,uf=`null`)|Cidade nova. Tentativa de criação. Criacao falhou: dados obrigatorios nao informados|
      |2020-03-01T23:04:00|Novo tentativas_pagamento com (numpedido=10001,numtentativa_pagamento=9911,status=ok)|Tentativa de criacao. Criacao com sucesso|
      > O exemplo acima emula um log para análise posterior de problemas ou do que ocorreu com os registros sendo inseridos. Enriqueça-o como achar interessante. Ele deve conter no minimo a tupla que tentou ser inserida ou atualizada e o resultado dessa operaçao. O log é de nível técnico e pode conter dados de baixo nível
    - Você **não deve** apagar a tabela `logs_processamento_pedido` e recalcula-la por completo em nenhuma hipótese. Somente devem ser feitas adições nessa tabela.



## Anexos

### Anexo I - Estrutura da pedidos_recebidos

| field | type | comment |
|--|--|--|
`numpedido`|`number(20,2)`|Número do pedido. Unico
`total`|`number(20,2)`|Total final do pedido
`datapedido`|`date`| Data do pedido
`rua`|`varchar2(250)`| Rua / endereço de entrega
`numero`|`varchar2(15)`| Número / endereço de entrega
`cidade`|`varchar2(50)`| Cidade / endereço de entrega
`uf`|`varchar2(2)` | Unidade da Federação / endereço de entrega
`tentativas_pagamento`|`varchar2(250)`| Eventos de tentativa de pagamento

### Anexo II - Estrutura da cidades_com_pedidos

| field | type | comment |
|--|--|--|
`cidade`|`varchar2(50)`|Nome da cidade
`uf`|`varchar2(2)`|Unidade da Federação / endereço de entrega

### Anexo III - Estrutura da total_pedidos_data

| field | type | comment |
|--|--|--|
`data_dia`|`varchar2(50)`|Data do dia de venda. Deve ser normalizado para a meia-noite daquela data, horário de Brasilia (UTC-03 / BST)
`total_vendido`|`number(20,2)`| Total agregado de vendas no dia

### Anexo IV - Estrutura da tentativas_pagamento

| field | type | comment |
|--|--|--|
`numpedido`|`date`| Numero do pedido para o qual foi feita a tentativa de pagamento
`numtentativa_pagamento`|`varchar2(250)`| Numero de controle da tentativa de pagamento
`status`|`varchar2(5)`| Valores possiveis: 'ok' e 'error'

### Anexo V - Estutura da logs_processamento_pedido

| field | type | comment |
|--|--|--|
`data_log`|`date`|Data do dia de venda. Deve ser normalizado para a meia-noite daquela data, horário de Brasilia (UTC-03 / BST)
`texto_operacao`|`varchar2(250)`| Total agregado de vendas no dia
`resultado_operacao`|`varchar2(500)`| Resultado da operacao e demais detalhes pertinentes àquele log.